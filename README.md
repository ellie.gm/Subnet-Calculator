## Subnet Calculator

Subnet Calculator program has been written in Python 2. This program receives the IP address and Subnetmask
as input from the user and calculates the following outputs:

 - The network address
 - The broadcast address
 - The number of valid hosts per subnet
 - The number of mask bits in CIDR notation
 - The wildcard mask

