import sys


def subnet_calculator():
    try:
        ######### Checking IP address validity ##########

        while True:
            # receives IP address from user
            ipaddress = raw_input("\nPlease enter an IP address: ")

            # splits the octets of IP address.
            i = ipaddress.split('.')
            # print i

            # checks the validity of the splitted IP address octets; every valid IP address has 4 length
            if (len(i) == 4) and (1 <= int(i[0]) <= 223) and (int(i[0]) != 127) and (
                    0 <= int(i[1]) <= 255 and 0 <= int(i[2]) <= 255 and 0 <= int(i[3]) <= 255):
                break

            else:
                print
                "\nThe IP address is INVALID, please try again!!!"
                continue

        ######### Checking subnetmask validity #########

        masks = [255, 254, 252, 248, 240, 224, 192, 128, 0]
        while True:
            # receives subnetmask from user
            subnetmask = raw_input("\nPlease enter a subnetmask: ")

            # Splits the octets of subnetmask.
            s = subnetmask.split('.')
            # print s

            # checks the validity of the splitted subnetmasks octet; every valid subnetmask has 4 length
            # and every octet must be in [255, 254, 252, 248, 240, 224, 192, 128, 0]
            # and every octet from left to right must be higher or equal to the other octets
            if (len(s) == 4) and (int(s[0]) in masks) and (int(s[1]) in masks) and (int(s[2]) in masks) and (
                    int(s[3]) in masks) and (int(s[0]) >= int(s[1]) >= int(s[2]) >= int(s[3])):
                break

            else:
                print
                "\nThe subnetmask is INVALID, please try again!!!"
                continue

        ######### Converting subnetmask to binary string #########

        # Splits the octets of subnetmask.
        mask_decimal_octet = subnetmask.split(".")
        # print mask_decimal_octet

        mask_octets = []

        for octet in range(0, len(mask_decimal_octet)):

            # print bin(int(mask_decimal_octet[octet])): casts splitted octets of subnetmask to binary integer
            # the result for every octet contains "b" character. Therefore, split("b")[1] will remove the "b".
            mask_binary_octet = bin(int(mask_decimal_octet[octet])).split("b")[1]
            # print mask_binary_octet

            # every binary octet must have 8 bits.
            if len(mask_binary_octet) == 8:
                mask_octets.append(mask_binary_octet)

            # In case of "0" number in decimal octet, and casting it to binary,
            # the binary octet contains just one "0" bit. Therefore, mask_binary_octet.zfill(8) will add extra "0" to the given octet
            elif len(mask_binary_octet) < 8:
                mask_binary_completedoctet = mask_binary_octet.zfill(8)
                mask_octets.append(mask_binary_completedoctet)

        # print mask_octets
        mask = "".join(mask_octets)
        # print mask

        ########## Calculating number of hosts/subnet based on subnetmask binary notation  #########

        # counts the number of zeros in subnetmask binary
        no_zeros = mask.count("0")
        # print no_zeros

        # number of ones in subnetmask is: 32 minus number of zeros
        no_ones = 32 - no_zeros
        # print no_ones

        # in every network number of hosts is: (2 power number of zeros) minus 2
        no_hosts = (2 ** no_zeros) - 2
        # print no_hosts

        ######### Calculating wildcardmask based on subnetmask decimal notation #########

        wildcard_octets = []
        for octets in mask_decimal_octet:
            # the formula of wildcardmask for every octet is: 255 minus octet
            wildcard_octet = 255 - int(octets)

            # appends every string octet of wildcardmask to wildcard_octets list.
            wildcard_octets.append(str(wildcard_octet))
            # print wildcard_octet

        # the complete octets of wildcardmask
        wildcardmask = ".".join(wildcard_octets)
        # print wildcardmask

        ######### Converting IP address to binary string #########

        # Splits the octets of IP address.
        ip_decimal_octets = ipaddress.split(".")
        ip_octets = []

        for octets in range(0, len(ip_decimal_octets)):
            # print bin(int(ip_decimal_octets[octets])) : casts splitted octets of IP address to binary integer
            # result for every octet contains "b" character. Therefore, split("b")[1] will remove the "b".
            ip_binary_octet = bin(int(ip_decimal_octets[octets])).split("b")[1]

            # print ip_binary_octet

            # every binary octet must have 8 bits.
            if len(ip_binary_octet) == 8:
                ip_octets.append(ip_binary_octet)

            # In case of "0" number in decimal octet, and casting it to binary,
            # the binary octet contains just one "0" bit. Therefore, ip_binary_octet.zfill(8) will add extra "0" to the given octet
            elif len(ip_binary_octet) < 8:
                ip_binary_completedoctet = ip_binary_octet.zfill(8)
                ip_octets.append(ip_binary_completedoctet)

        # print ip_octets
        ip_binary = "".join(ip_octets)
        # print ip_binary

        ######### calculating network address #########

        # calculates the network address binary based on number of "1" in subnetmask binary; seprates the network address bits from hosts bits
        # and changes host's bits in IP address to "0"
        networkaddress_binary = ip_binary[:(no_ones)] + "0" * no_zeros
        # print networkaddress_binary

        # separates every 8 bits of network address binary
        net_octets = []
        for octet in range(0, len(networkaddress_binary), 8):
            net_octet = networkaddress_binary[octet:octet + 8]
            net_octets.append(net_octet)
            # print net_octets

        # turns every 8 bits of network address binary to decimal
        networkaddress = []
        for octe in net_octets:
            networkaddress.append(str(int(octe, 2)))

            # print networkaddress
        # joins 4 octets together by "."
        network_address = ".".join(networkaddress)
        # print network_address

        ######### calculating broadcast address #########

        # calculates the broadcast address  binary based on number of "1" in subnetmask binary; separates the network address bits from hosts bits
        # and changes host's bits in IP address to "1"
        broadcastaddress_binary = ip_binary[:(no_ones)] + ("1" * no_zeros)
        # print broadcastaddress_binary

        # separates every 8 bits of network address binary
        broadcast_octets = []
        for octet in range(0, len(broadcastaddress_binary), 8):
            broadcast_octet = broadcastaddress_binary[octet:octet + 8]
            broadcast_octets.append(broadcast_octet)
        # print broadcast_octets

        # turns every 8 bits of network address binary to decimal
        broadcast_address = []
        for octet in broadcast_octets:
            broadcast_address.append(str(int(octet, 2)))

            # print broadcast_address
        # joins 4 octets together by "."
        broadcast_ip_address = ".".join(broadcast_address)
        # print broadcast_ip_address

        ######### Final result #########

        print
        "\n"
        print
        "The network address is: %s" % network_address
        print
        "The broadcast address is: %s" % broadcast_ip_address
        print
        "The number of valid hosts per subnet is: %s" % no_hosts
        print
        "The number of mask bits is: %s" % no_ones
        print
        "The wildcard mask: %s" % wildcardmask
        print
        "\n"


    except KeyboardInterrupt:
        print
        "/n Interruption /n"
        sys.exit()


subnet_calculator()